package jxo;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import jxo.winnercheck.*;

public class GameController {
    @FXML Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9;
    @FXML Label playerLable1, playerLable2, gameCodeLable;
    private Boolean [][] fields = new Boolean[3][3]; /// X is true & O is false 
    private boolean turn = true;

    private boolean gameOver(){
        GameOverComposition gameOverComposition = new GameOverComposition(fields);
        GameState gameState = gameOverComposition.report();
        if (!gameState.equals(GameState.RESUMING)){
            disableAll();
            switch (gameState) {
                case O_WIN:
                    gameCodeLable.setText("player O wins.");
                    break;
                case X_WIN:
                    gameCodeLable.setText("player X wins.");
                    break;
                case EQUALS:
                    gameCodeLable.setText("No Winner!");
                    break;
            }
            return true;
        }
        else
            return false;
    }

//    private boolean winnerCheck() {
//        try {
//            if (fields[0][0].equals(true) && fields[1][1].equals(true) && fields[2][2].equals(true)
//                    || fields[0][2].equals(true) && fields[1][1].equals(true) && fields[2][0].equals(true)
//                    || fields[0][0].equals(true) && fields[0][1].equals(true) && fields[0][2].equals(true)
//                    || fields[1][0].equals(true) && fields[1][1].equals(true) && fields[1][2].equals(true)
//                    || fields[2][0].equals(true) && fields[2][1].equals(true) && fields[2][2].equals(true)
//                    || fields[0][0].equals(true) && fields[1][0].equals(true) && fields[2][0].equals(true)
//                    || fields[0][1].equals(true) && fields[1][1].equals(true) && fields[2][1].equals(true)
//                    || fields[0][2].equals(true) && fields[1][2].equals(true) && fields[2][2].equals(true))
//            {
//                System.out.println("player X wins.");
//                disableAll();
//                gameCodeLable.setText("player X wins.");
//                return true;
//            }
//            if (fields[0][0].equals(false) && fields[1][1].equals(false) && fields[2][2].equals(false)
//                    || fields[0][2].equals(false) && fields[1][1].equals(false) && fields[2][0].equals(false)
//                    || fields[0][0].equals(false) && fields[0][1].equals(false) && fields[0][2].equals(false)
//                    || fields[1][0].equals(false) && fields[1][1].equals(false) && fields[1][2].equals(false)
//                    || fields[2][0].equals(false) && fields[2][1].equals(false) && fields[2][2].equals(false)
//                    || fields[0][0].equals(false) && fields[1][0].equals(false) && fields[2][0].equals(false)
//                    || fields[0][1].equals(false) && fields[1][1].equals(false) && fields[2][1].equals(false)
//                    || fields[0][2].equals(false) && fields[1][2].equals(false) && fields[2][2].equals(false))
//            {
//                System.out.println("player O wins.");
//                disableAll();
//                gameCodeLable.setText("player O wins.");
//                return true;
//            }
//            if (   !fields[0][0].equals(null) && !fields[0][1].equals(null) && !fields[0][2].equals(null)
//                    && !fields[1][0].equals(null) && !fields[1][1].equals(null) && !fields[1][2].equals(null)
//                    && !fields[2][0].equals(null) && !fields[2][1].equals(null) && !fields[2][2].equals(null))
//            {
//                System.out.println("Mosavi");
//                gameCodeLable.setText("No Winner!");
//                disableAll();
//                return true;
//            }
//        }catch (Exception e){}
//        return false;
//    }

    public void disableAll (){
        btn1.setDisable(true);
        btn2.setDisable(true);
        btn3.setDisable(true);
        btn4.setDisable(true);
        btn5.setDisable(true);
        btn6.setDisable(true);
        btn7.setDisable(true);
        btn8.setDisable(true);
        btn9.setDisable(true);

    }

    public void btn1Click(MouseEvent mouseEvent) {
        btnClick(btn1, 0);
    }

    public void btn2Click(MouseEvent mouseEvent) {
        btnClick(btn2, 1);
    }

    public void btn3Click(MouseEvent mouseEvent) {
        btnClick(btn3, 2);
    }

    public void btn4Click(MouseEvent mouseEvent) {
        btnClick(btn4, 3);
    }

    public void btn5Click(MouseEvent mouseEvent) {
        btnClick(btn5, 4);
    }

    public void btn6Click(MouseEvent mouseEvent) {
        btnClick(btn6, 5);
    }

    public void btn7Click(MouseEvent mouseEvent) {
        btnClick(btn7, 6);
    }

    public void btn8Click(MouseEvent mouseEvent) {
        btnClick(btn8, 7);
    }

    public void btn9Click(MouseEvent mouseEvent) {
        btnClick(btn9, 8);
    }

    private void btnClick (Button btn, int pos){
        if (turn){
            btn.setDisable(true);
            btn.setText("X");
            fields[pos / 3][pos % 3] = new Boolean(true);
            playerLable2.setTextFill(Color.web("#ee4040"));
            playerLable1.setTextFill(Color.web("#000000"));
            turn = false;
        }
        else {
            btn.setDisable(true);
            btn.setText("O");
            fields[pos / 3][pos % 3] = new Boolean(false);
            playerLable1.setTextFill(Color.web("#ee4040"));
            playerLable2.setTextFill(Color.web("#000000"));
            turn = true;
        }
        gameOver();
    }

    public void ai (){

    }
}
