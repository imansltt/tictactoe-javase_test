package jxo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    @FXML
    TextField p1Field, p2Field, gameCodeField;
    @FXML
    Button startBtn, logBtn;
    @FXML
    ComboBox<String> gameMode;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBoxinit();
    }

    private void comboBoxinit (){
        ObservableList<String> obsList = FXCollections.observableArrayList();
        obsList.add("Human to Human");
        obsList.add("Human to AI");
        gameMode.setItems(obsList);
        gameMode.setValue("Human to Human");
    }

    public void comboChange(ActionEvent actionEvent) {
        if (gameMode.getValue().equals("Human to Human"))
            p2Field.setDisable(false);
        else
            p2Field.setDisable(true);
    }

    public void startGame(MouseEvent mouseEvent) {

    }
}
