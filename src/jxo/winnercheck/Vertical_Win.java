package jxo.winnercheck;

public class Vertical_Win extends GameOver {

    public Vertical_Win(Boolean[][] fields) {
        super(fields);
    }

    @Override
    public boolean check() {
        Boolean [][] fields = this.getFields();
        boolean gameOver = false;
        try{
            for (int i = 0; i < fields.length; i++) {
                if (fields[i][0] != null && fields[i][1] != null && fields[i][2] != null) {
                    if (fields[i][0].equals(true) && fields[i][1].equals(true) && fields[i][2].equals(true)){
                        this.setWhoWins(true);
                        gameOver = true;
                    }
                    if (fields[i][0].equals(false) && fields[i][1].equals(false) && fields[i][2].equals(false)){
                        this.setWhoWins(false);
                        gameOver = true;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return gameOver;
    }
}
