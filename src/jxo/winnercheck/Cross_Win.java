package jxo.winnercheck;

public class Cross_Win extends GameOver {

    public Cross_Win(Boolean[][] fields) {
        super(fields);
    }

    @Override
    public boolean check() {
        Boolean [][] fields = this.getFields();
        boolean gameOver = false;
        try{
            // an exception must be fix.
            // null pointer exception
            //convert || to && in first if
            //or
            //split second if
            if (fields[0][0] != null && fields[1][1] != null && fields[2][2] != null
                    || fields[0][2] != null && fields[1][1] != null && fields[2][0] != null){
                if (fields[0][0].equals(false) && fields[1][1].equals(false) && fields[2][2].equals(false)
                        || fields[0][2].equals(false) && fields[1][1].equals(false) && fields[2][0].equals(false)){
                    gameOver = true;
                    this.setWhoWins(false);
                }

                if (fields[0][0].equals(true) && fields[1][1].equals(true) && fields[2][2].equals(true)
                        || fields[0][2].equals(true) && fields[1][1].equals(true) && fields[2][0].equals(true)){
                    gameOver = true;
                    this.setWhoWins(true);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return gameOver;
    }

}
