package jxo.winnercheck;

public enum GameOverChilds {
    HORIZONTAL_WIN,
    VERTICAL_WIN,
    CROSS_WIN,
    EQUALS,
    NULL
}
