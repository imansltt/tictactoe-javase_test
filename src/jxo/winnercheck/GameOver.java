package jxo.winnercheck;

public abstract class GameOver {
    private Boolean [][] fields;
    private Boolean whoWins = null;

    public GameOver(Boolean[][] fields) {
        this.fields = fields;
    }

    public Boolean[][] getFields() {
        return fields;
    }

    public void setFields(Boolean[][] fields) {
        this.fields = fields;
    }

    public void setWhoWins(Boolean whoWins) {
        this.whoWins = whoWins;
    }

    ///whoWins Getter
    public boolean who_wins(){
        return whoWins;
    }

    public abstract boolean check();

}
