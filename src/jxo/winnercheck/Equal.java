package jxo.winnercheck;

public class Equal extends GameOver {

    public Equal(Boolean[][] fields) {
        super(fields);
    }

    @Override
    public boolean check() {
        Boolean [][] fields = this.getFields();
        boolean gameOver = false;
        try{
            if (fields[0][0] != null && fields[0][1] != null && fields[0][2] != null
                    && fields[1][0 ]!= null && fields[1][1] != null && fields[1][2] != null
                    && fields[2][0] != null && fields[2][1] != null && fields[2][2] != null){
                gameOver = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return gameOver;
    }
}
