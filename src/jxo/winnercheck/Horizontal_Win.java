package jxo.winnercheck;

public class Horizontal_Win extends GameOver {

    public Horizontal_Win(Boolean[][] fields) {
        super(fields);
    }

    @Override
    public boolean check() {
        Boolean [][] fields = this.getFields();
        boolean gameOver = false;
        try{
            for (int i = 0; i < fields.length; i++) {
                if ( fields[0][i] != null && fields[1][i] != null && fields[2][i] != null) {
                    if (fields[0][i].equals(true) && fields[1][i].equals(true) && fields[2][i].equals(true)) {
                        this.setWhoWins(true);
                        gameOver = true;
                    }
                    if (fields[0][i].equals(false) && fields[1][i].equals(false) && fields[2][i].equals(false)){
                        this.setWhoWins(false);
                        gameOver = true;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return gameOver;
    }

}
