package jxo.winnercheck;

public enum GameState {
    X_WIN,
    O_WIN,
    EQUALS,
    RESUMING
}
