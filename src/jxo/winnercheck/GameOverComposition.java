package jxo.winnercheck;

import java.util.HashMap;

public class GameOverComposition {
    HashMap<GameOverChilds, GameOver> gameOver = new HashMap<>();
    public GameOverComposition (Boolean[][] fields){
        gameOver.put(GameOverChilds.HORIZONTAL_WIN, new Horizontal_Win(fields));
        gameOver.put(GameOverChilds.VERTICAL_WIN, new Vertical_Win(fields));
        gameOver.put(GameOverChilds.CROSS_WIN, new Cross_Win(fields));
        gameOver.put(GameOverChilds.EQUALS, new Equal(fields));
    }

    public GameState report(){
        if (gameOver.get(GameOverChilds.HORIZONTAL_WIN).check())
            if (gameOver.get(GameOverChilds.HORIZONTAL_WIN).who_wins())
                return GameState.X_WIN;
            else
                return GameState.O_WIN;
        if (gameOver.get(GameOverChilds.VERTICAL_WIN).check())
            if (gameOver.get(GameOverChilds.VERTICAL_WIN).who_wins())
                return GameState.X_WIN;
            else
                return GameState.O_WIN;
        if (gameOver.get(GameOverChilds.CROSS_WIN).check())
            if (gameOver.get(GameOverChilds.CROSS_WIN).who_wins())
                return GameState.X_WIN;
            else
                return GameState.O_WIN;
        if (gameOver.get(GameOverChilds.EQUALS).check())
            return GameState.EQUALS;
        return GameState.RESUMING;
    }
}
